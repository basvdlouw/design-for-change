# Design for change sep 2021

Samenwerken tussen Bas van de Louw & Brent Van Handenhove

## Build

Prerequisites: Java 1.8, Maven

Build with 
```console
mvn package
```

Currently built with Java 1.8 compiler. Can be configured in pom.xml to use a higher version.




## Run 

run with 
```console
java -jar target/design-for-change-sep-2021-1.0-SNAPSHOT.jar
```

## Eclipse

### Import project: 

File -> Import -> Maven -> Existing Maven Projects -> design-for-change

Select /pom.xml and import.

### Run:

src/main/java/Jabberpoint.java -> Run As: Java Application
