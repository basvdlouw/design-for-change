package view.controllers;

import view.events.InputEventManager;

import javax.swing.*;
import java.awt.*;

/**
 * <p>De controller voor het menu</p>
 *
 * @author Ian F. Darwin, ian@darwinsys.com, Gert Florijn, Sylvia Stuurman
 * @version 1.6 2014/05/16 Sylvia Stuurman
 */
public class MenuController extends MenuBar {

    protected static final String ABOUT = "About";
    protected static final String FILE = "File";
    protected static final String EXIT = "Exit";
    protected static final String GOTO = "Go to";
    protected static final String HELP = "Help";
    protected static final String NEW = "New";
    protected static final String NEXT = "Next";
    protected static final String OPEN = "Open";
    protected static final String PAGENR = "Page number?";
    protected static final String PREV = "Prev";
    protected static final String SAVE = "Save";
    protected static final String VIEW = "View";
    protected static final String TESTFILE = "test.xml";
    protected static final String SAVEFILE = "dump.xml";
    protected static final String IOEX = "IO Exception: ";
    protected static final String LOADERR = "Load Error";
    protected static final String SAVEERR = "Save Error";
    private static final long serialVersionUID = 227L;

    final InputEventManager eventManager;

    //TODO: Add general Accessor interface. Make XMLAccessor implement load/save file
    public MenuController(final InputEventManager eventManager) {
        MenuItem menuItem;

        final Menu fileMenu = new Menu(FILE);
        fileMenu.add(menuItem = makeMenuItem(OPEN));
        menuItem.addActionListener(actionEvent -> loadFileAction());
        fileMenu.add(menuItem = makeMenuItem(SAVE));
        menuItem.addActionListener(actionEvent -> saveFileAction());
        fileMenu.addSeparator();
        fileMenu.add(menuItem = makeMenuItem(EXIT));
        menuItem.addActionListener(actionEvent -> stopSlideShowAction());
        add(fileMenu);

        final Menu viewMenu = new Menu(VIEW);
        viewMenu.add(menuItem = makeMenuItem(NEXT));
        menuItem.addActionListener(actionEvent -> nextSlideAction());
        viewMenu.add(menuItem = makeMenuItem(PREV));
        menuItem.addActionListener(actionEvent -> previousSlideAction());
        viewMenu.add(menuItem = makeMenuItem(GOTO));
        menuItem.addActionListener(actionEvent -> gotoSlideAction());
        add(viewMenu);

        this.eventManager = eventManager;
    }

    public MenuItem makeMenuItem(final String name) {
        return new MenuItem(name, new MenuShortcut(name.charAt(0)));
    }

    private void loadFileAction() {
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showOpenDialog(null);

        if (returnVal != JFileChooser.APPROVE_OPTION) {
            System.out.println("Open command cancelled by user." + "\n");
            return;
        }

        final String filename = fc.getSelectedFile().getName();
        eventManager.loadSlideShowEventDispatcher.loadSlideShow(this, filename);
    }

    private void saveFileAction() {
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(null);

        if (returnVal != JFileChooser.APPROVE_OPTION) {
            System.out.println("Save command cancelled by user." + "\n");
            return;
        }

        final String filename = fc.getSelectedFile().getAbsolutePath();
        eventManager.saveSlideShowEventDispatcher.saveSlideShow(this, filename);
    }

    private void stopSlideShowAction() {
        eventManager.stopSlideShowEventDispatcher.slideShowStopped(this);
    }

    private void nextSlideAction() {
        eventManager.nextSlideEventDispatcher.nextSlide(this);
    }

    private void previousSlideAction() {
        eventManager.previousSlideEventDispatcher.previousSlide(this);
    }

    private void gotoSlideAction() {
        final String pageNumberStr = JOptionPane.showInputDialog(PAGENR);
        final int pageNumber = Integer.parseInt(pageNumberStr);
        eventManager.navigateToEventDispatcher.goToSlide(this, pageNumber - 1);
    }
}
