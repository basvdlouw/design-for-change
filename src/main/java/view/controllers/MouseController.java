package view.controllers;

import view.component.ActionComponent;
import view.ui.SlideViewerComponent;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseController implements MouseListener {

    private final SlideViewerComponent slideViewerComponent;

    public MouseController(final SlideViewerComponent slideViewerComponent) {
        this.slideViewerComponent = slideViewerComponent;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        Point pressPoint = e.getPoint();

        for (Component c : slideViewerComponent.getComponents()) {
            if (c instanceof ActionComponent) {
                ActionComponent actionComponent = (ActionComponent) c;
                if (actionComponent.getBoundingBox().contains(pressPoint)) {
                    actionComponent.OnClick();
                }
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
