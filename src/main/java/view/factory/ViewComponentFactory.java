package view.factory;

import domain.components.composite.slide.Slide;
import domain.components.items.ISlideItem;
import view.component.UIComponent;

import java.util.Collection;

public interface ViewComponentFactory {

    Collection<UIComponent<? extends ISlideItem>> getViewComponents(final Slide slide);
}
