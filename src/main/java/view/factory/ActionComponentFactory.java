package view.factory;

import domain.components.ISlideShowComponent;
import domain.components.composite.ISlideShowComposite;
import domain.components.items.SlideItem;
import domain.components.items.SlideItemActionDecorator;
import domain.components.items.figure.FigureItem;
import domain.components.items.text.TextItem;
import domain.components.visit.visitor.Visitor;
import view.component.ActionComponent;
import view.component.UIComponent;

public class ActionComponentFactory implements Visitor {

    private UIComponent<? extends SlideItem> uiComponent;
    private final ItemComponentFactory itemComponentFactory;

    public ActionComponentFactory() {
        this.itemComponentFactory = new ItemComponentFactory();
    }

    public ActionComponent createActionComponent(final SlideItemActionDecorator slideItemActionDecorator) {
        slideItemActionDecorator.accept(this);
        return new ActionComponent(slideItemActionDecorator, this.uiComponent);
    }

    @Override
    public void visit(final SlideItemActionDecorator slideItemActionDecorator) {
        slideItemActionDecorator.getSlideItem().accept(this);
    }

    @Override
    public void visit(final FigureItem figureItem) {
        this.uiComponent = this.itemComponentFactory.createFigureComponent(figureItem);
    }

    @Override
    public void visit(final TextItem textItem) {
        this.uiComponent = this.itemComponentFactory.createTextComponent(textItem);
    }

    @Override
    public void visit(final ISlideShowComposite<? extends ISlideShowComponent> composite) {
        throw new IllegalArgumentException("Action factory shouldn't visit any composites");
    }
}
