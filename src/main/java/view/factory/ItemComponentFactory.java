package view.factory;

import domain.components.items.figure.FigureItem;
import domain.components.items.text.TextItem;
import view.component.FigureComponent;
import view.component.TextComponent;

public class ItemComponentFactory {

    public FigureComponent createFigureComponent(final FigureItem figureItem) {
        return new FigureComponent(figureItem);
    }

    public TextComponent createTextComponent(final TextItem textItem) {
        return new TextComponent(textItem);
    }
}
