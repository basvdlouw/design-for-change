package view.factory;

import domain.components.ISlideShowComponent;
import domain.components.composite.ISlideShowComposite;
import domain.components.composite.slide.Slide;
import domain.components.items.ISlideItem;
import domain.components.items.SlideItemActionDecorator;
import domain.components.items.figure.FigureItem;
import domain.components.items.text.TextItem;
import domain.components.visit.visitor.Visitor;
import view.component.UIComponent;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Factory that returns a List of UIComponents based on Slide input. Uses Visitor pattern to traverse the Slide composite structure.
 */
public class UIComponentFactory implements Visitor, ViewComponentFactory {

    private Collection<UIComponent<? extends ISlideItem>> generatedUIComponents;
    private final ActionComponentFactory actionComponentFactory;
    private final ItemComponentFactory itemComponentFactory;

    public UIComponentFactory() {
        this.clear();
        this.actionComponentFactory = new ActionComponentFactory();
        this.itemComponentFactory = new ItemComponentFactory();
    }

    private void clear() {
        this.generatedUIComponents = new ArrayList<>();
    }

    @Override
    public void visit(final ISlideShowComposite<? extends ISlideShowComponent> composite) {
        composite.getComponents().forEach(component -> component.accept(this));
    }

    @Override
    public void visit(final TextItem textItem) {
        this.generatedUIComponents.add(
                this.itemComponentFactory.createTextComponent(textItem)
        );
    }

    @Override
    public void visit(final FigureItem figureItem) {
        this.generatedUIComponents.add(
                this.itemComponentFactory.createFigureComponent(figureItem)
        );
    }

    @Override
    public void visit(final SlideItemActionDecorator slideItemActionDecorator) {
        this.generatedUIComponents.add(
                this.actionComponentFactory.createActionComponent(slideItemActionDecorator)
        );
    }

    @Override
    public Collection<UIComponent<? extends ISlideItem>> getViewComponents(final Slide slide) {
        slide.getComponents().forEach(component -> component.accept(this));
        final Collection<UIComponent<? extends ISlideItem>> items = this.generatedUIComponents;
        this.clear();
        return items;
    }
}
