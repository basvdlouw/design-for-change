package view.component;

import domain.components.items.style.Style;

import java.awt.*;
import java.awt.image.ImageObserver;

public interface ViewComponent {

    Rectangle getBoundingBox();

    void draw(final int x, final int y, final float scale, final Graphics g, final Style style, final ImageObserver observer);
}

