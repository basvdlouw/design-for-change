package view.component;

import domain.components.items.ISlideItem;
import domain.components.items.style.Style;

import java.awt.*;
import java.awt.image.ImageObserver;

public abstract class UIComponent<T extends ISlideItem> extends Component implements ViewComponent {
	private static final long serialVersionUID = -7085773184058802730L;
	protected final T slideItem;
    protected Rectangle boundingBox;
    protected Point position;

    public UIComponent(final T slideItem) {
        this.slideItem = slideItem;
        this.boundingBox = new Rectangle();
        this.position = new Point();
    }

    public abstract Rectangle getLocalBoundingBox(final Graphics g, final ImageObserver observer, final float scale, final Style style);

    @Override
    public Rectangle getBoundingBox() {
        return new Rectangle(boundingBox.x + position.x, boundingBox.y + position.y, boundingBox.width, boundingBox.height);
    }

    @Override
    public void draw(final int x, final int y, final float scale, final Graphics g, final Style style, final ImageObserver observer) {
        boundingBox = getLocalBoundingBox(g, observer, scale, style);
        position.x = x;
        position.y = y;
    }

    public Style getStyle() {
        return Style.getStyle(this.slideItem.getLevel().getLevel());
    }
}
