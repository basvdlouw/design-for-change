package view.component;

import domain.components.items.style.Style;
import domain.components.items.text.TextItem;
import view.ui.SlideViewerFrame;

import java.awt.*;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.awt.image.ImageObserver;
import java.text.AttributedString;
import java.util.ArrayList;
import java.util.List;

public class TextComponent extends UIComponent<TextItem> {
	private static final long serialVersionUID = 5130417938499047685L;

	public TextComponent(final TextItem textItem) {
        super(textItem);
    }

    private AttributedString getAttributedString(final Style style, final float scale) {
        String text = this.slideItem.getTextData().getText();

        // If text is left empty in the xml, AttributedString will be null
        // To prevent this, we'll set an empty string to be " " instead
        if (text == "") { text = " "; }

        final AttributedString attrStr = new AttributedString(text);
        attrStr.addAttribute(TextAttribute.FONT, style.getFont(scale), 0, text.length());
        return attrStr;
    }

    private List<TextLayout> getLayouts(final Graphics g, final Style s, final float scale) {
        final List<TextLayout> layouts = new ArrayList<>();
        final Graphics2D g2d = (Graphics2D) g;
        final LineBreakMeasurer measurer = new LineBreakMeasurer(getAttributedString(s, scale).getIterator(), g2d.getFontRenderContext());
        final float wrappingWidth = (SlideViewerFrame.WIDTH - s.getIndent()) * scale;
        while (measurer.getPosition() < this.slideItem.getTextData().getText().length()) {
            final TextLayout layout = measurer.nextLayout(wrappingWidth);
            layouts.add(layout);
        }
        return layouts;
    }

    @Override
    public Rectangle getLocalBoundingBox(final Graphics g, final ImageObserver observer, final float scale, final Style style) {
        final List<TextLayout> layouts = getLayouts(g, style, scale);
        int xsize = 0, ysize = 0;

        for (int i = 0; i < layouts.size(); i++) {
            final TextLayout layout = layouts.get(i);
            final Rectangle2D bounds = layout.getBounds();
            if (bounds.getWidth() > xsize) {
                xsize = (int) bounds.getWidth();
            }
            if (bounds.getHeight() > 0) {
                ysize += bounds.getHeight();
            }
            if (i != (layouts.size() - 1))
            {
                ysize += layout.getLeading();
            }
            ysize += layout.getDescent();
        }
        return new Rectangle((int) (style.getIndent() * scale), (int) (style.getLeading() * scale), xsize, ysize);
    }

    @Override
    public void draw(final int x, final int y, final float scale, final Graphics g, final Style style, final ImageObserver observer) {
        super.draw(x, y, scale, g, style, observer);

        final String text = this.slideItem.getTextData().getText();
        if (text == null || text.length() == 0) {
            return;
        }
        final Rectangle boundingBox = this.getLocalBoundingBox(g, observer, scale, style);
        final List<TextLayout> layouts = getLayouts(g, style, scale);
        final Point pen = new Point(x + boundingBox.x, y + boundingBox.y);
        final Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(style.getColor());
        layouts.forEach(layout -> {
            pen.y += layout.getAscent();
            layout.draw(g2d, pen.x, pen.y);
            pen.y += layout.getDescent();
        });
    }
}
