package view.component;

import domain.components.items.figure.FigureItem;
import domain.components.items.style.Style;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

public class FigureComponent extends UIComponent<FigureItem> {
	private static final long serialVersionUID = -4886381596361161301L;

	public FigureComponent(final FigureItem figureItem) {
        super(figureItem);
    }

    @Override
    public Rectangle getLocalBoundingBox(final Graphics g, final ImageObserver observer, final float scale, final Style style) {
        final BufferedImage figure = this.slideItem.getFigure();
        return new Rectangle((int) (style.getIndent() * scale),
                (int) (style.getLeading() * scale),
                (int) (figure.getWidth(observer) * scale),
                (int) (figure.getHeight(observer) * scale)
        );
    }

    @Override
    public void draw(int x, int y, final float scale, final Graphics g, final Style style, final ImageObserver observer) {
        super.draw(x, y, scale, g, style, observer);

        final BufferedImage figure = this.slideItem.getFigure();
        final Rectangle boundingBox = this.getLocalBoundingBox(g, observer, scale, style);
        x += boundingBox.x;
        y += boundingBox.y;
        g.drawImage(figure, x, y, boundingBox.width, boundingBox.height, observer);
    }
}
