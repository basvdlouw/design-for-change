package view.component;

import domain.components.items.SlideItem;
import domain.components.items.SlideItemActionDecorator;
import domain.components.items.style.Style;

import java.awt.*;
import java.awt.image.ImageObserver;

public class ActionComponent extends UIComponent<SlideItemActionDecorator> {
	private static final long serialVersionUID = 3524428939369387517L;
	private final UIComponent<? extends SlideItem> uiComponent;

    /**
     * ActionComponent is a wrapper around another UIComponent.
     *
     * @param actionDecorator action properties
     * @param uiComponent     component that is being wrapped around
     */
    public ActionComponent(final SlideItemActionDecorator actionDecorator, final UIComponent<? extends SlideItem> uiComponent) {
        super(actionDecorator);
        this.uiComponent = uiComponent;
    }

    public void OnClick() {
        slideItem.onAction();
    }

    @Override
    public Rectangle getLocalBoundingBox(final Graphics g, final ImageObserver observer, final float scale, final Style style) {
        return this.uiComponent.getLocalBoundingBox(g, observer, scale, style);
    }

    @Override
    public void draw(final int x, final int y, final float scale, final Graphics g, final Style style, final ImageObserver observer) {
        super.draw(x, y, scale, g, style, observer);
        this.uiComponent.draw(x, y, scale, g, style, observer);

        final Rectangle itemBB = this.uiComponent.getBoundingBox();
        //itemBB.x += x;
        //itemBB.y += y;
        final int thickness = 2;
        final int[] xPoints = {itemBB.x, itemBB.x + itemBB.width, itemBB.x + itemBB.width, itemBB.x, itemBB.x, itemBB.x + thickness, itemBB.x + thickness, itemBB.x + itemBB.width - thickness, itemBB.x + itemBB.width - thickness, itemBB.x};
        final int[] yPoints = {itemBB.y, itemBB.y, itemBB.y + itemBB.height, itemBB.y + itemBB.height, itemBB.y + thickness, itemBB.y + thickness, itemBB.y + itemBB.height - thickness, itemBB.y + itemBB.height - thickness, itemBB.y + thickness, itemBB.y + thickness};
        g.setColor(Color.RED);
        g.fillPolygon(xPoints, yPoints, 10);
    }
}

