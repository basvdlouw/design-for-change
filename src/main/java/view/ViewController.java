package view;

import view.controllers.KeyController;
import view.controllers.MenuController;
import application.ApplicationController;
import view.events.InputEventManager;
import view.ui.ISlideShowViewComponent;
import view.ui.SlideViewerFrame;

public class ViewController {

    private final ApplicationController applicationController;
    private final InputEventManager inputEventManager;
    private final ISlideShowViewComponent viewComponent;

    public ViewController(final ApplicationController applicationController) {
        this.applicationController = applicationController;
        this.inputEventManager = new InputEventManager();
        final KeyController keyController = new KeyController(this.inputEventManager);
        final MenuController menuController = new MenuController(this.inputEventManager);
        this.viewComponent = new SlideViewerFrame(
                ApplicationController.TITLE,
                keyController,
                menuController
        );

        setupUIEventListeners();
        setupInputEventListeners();
    }

    /**
     * UI listens for application layer domain.events
     */
    private void setupUIEventListeners() {
        this.applicationController.getEventManager().startSlideShowEventDispatcher.addListener(this.viewComponent);
        this.applicationController.getEventManager().slideChangedEventDispatcher.addListener(this.viewComponent);
        this.applicationController.getEventManager().stopSlideShowEventDispatcher.addListener(this.viewComponent);
    }

    /**
     * Listen for input domain.events and call application layer accordingly
     */
    private void setupInputEventListeners() {
        this.inputEventManager.loadSlideShowEventDispatcher.addListener(
                event -> this.applicationController.start(event.getFilename())
        );
        this.inputEventManager.stopSlideShowEventDispatcher.addListener(
                event -> this.applicationController.stop()
        );
        this.inputEventManager.saveSlideShowEventDispatcher.addListener(
                event -> this.applicationController.save(event.getFilename())
        );
        this.inputEventManager.nextSlideEventDispatcher.addListener(
                event -> this.applicationController.next()
        );
        this.inputEventManager.previousSlideEventDispatcher.addListener(
                event -> this.applicationController.prev()
        );
        this.inputEventManager.navigateToEventDispatcher.addListener(
                event -> this.applicationController.goTo(event.getPosition())
        );
    }
}
