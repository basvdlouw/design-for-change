package view.ui;

import domain.components.composite.slide.Slide;
import domain.components.items.ISlideItem;
import domain.components.items.style.Style;
import view.component.UIComponent;
import view.factory.UIComponentFactory;
import view.factory.ViewComponentFactory;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.Collection;


/**
 * <p>SlideViewerComponent is een grafische component die Slides kan laten zien.</p>
 *
 * @author Ian F. Darwin, ian@darwinsys.com, Gert Florijn, Sylvia Stuurman
 * @version 1.6 2014/05/16 Sylvia Stuurman
 */

public class SlideViewerComponent extends JComponent {
	private static final long serialVersionUID = 8997514091544659397L;
	private final Font labelFont; // het font voor labels
    private Slide slide;
    private final ViewComponentFactory viewComponentFactory;
    private static final Color BGCOLOR = Color.white;
    private static final Color COLOR = Color.black;
    private static final String FONTNAME = "Dialog";
    private static final int FONTSTYLE = Font.BOLD;
    private static final int FONTHEIGHT = 10;
    private static final int XPOS = 1100;
    private static final int YPOS = 20;

    private int presentationSize;

    public SlideViewerComponent() {
        setBackground(BGCOLOR);
        this.labelFont = new Font(FONTNAME, FONTSTYLE, FONTHEIGHT);
        this.viewComponentFactory = new UIComponentFactory(); // Inject this factory using dependency injection
    }

    private void addComponents() {
        this.createViewComponents(slide).forEach(this::add);
    }

    private void removeComponents() {
        Arrays.stream(this.getComponents()).forEach(this::remove);
    }

    private Collection<UIComponent<? extends ISlideItem>> createViewComponents(final Slide slide) {
        return this.viewComponentFactory.getViewComponents(slide);
    }

    private void initializeComponents() {
        // Remove components from the previous slide and initialize the new slide.
        this.removeComponents();
        this.addComponents();
    }

    public Dimension getPreferredSize() {
        return new Dimension(SlideViewerFrame.WIDTH, SlideViewerFrame.HEIGHT);
    }

    public void start(final Slide slide, final int presentationSize) {
        this.presentationSize = presentationSize;
        this.update(slide);
    }

    public void update(final Slide slide) {
        if (slide == null) {
            repaint();
            return;
        }
        this.slide = slide;
        this.initializeComponents();
        repaint();
    }

    @Override
    public void paintComponent(final Graphics g) {
        g.setColor(BGCOLOR);
        g.fillRect(0, 0, getSize().width, getSize().height);
        if (this.slide == null) {
            return;
        }
        g.setFont(labelFont);
        g.setColor(COLOR);
        g.drawString("Slide " + (this.slide.getSequenceNumber()) + " of " +
                this.presentationSize, XPOS, YPOS);
        final Rectangle area = new Rectangle(0, YPOS, getWidth(), (getHeight() - YPOS));
        this.drawUIComponents(g, area);
    }

    private float getScale(final Rectangle area) {
        return Math.min(((float) area.width) / ((float) SlideViewerFrame.WIDTH), ((float) area.height) / ((float) SlideViewerFrame.HEIGHT));
    }

    private void drawUIComponents(final Graphics g, final Rectangle area) {
        final float scale = getScale(area);
        final int x = area.x;
        int y = area.y;
        for (final Component component : this.getComponents()) {
            if (component instanceof UIComponent<?>) {
                final UIComponent<?> uiComponent = (UIComponent<?>) component;
                final Style style = uiComponent.getStyle();
                final Rectangle boundingBox = uiComponent.getLocalBoundingBox(g, this, scale, style);
                uiComponent.draw(x, y, scale, g, style, this);
                y += boundingBox.y + boundingBox.height;
            }
        }
    }
}
