package view.ui;

import domain.events.stop.StopSlideShowEventListener;
import domain.events.start.StartSlideShowEventListener;
import domain.events.update.SlideChangedEventListener;

public interface ISlideShowViewComponent extends StartSlideShowEventListener, SlideChangedEventListener, StopSlideShowEventListener {
}
