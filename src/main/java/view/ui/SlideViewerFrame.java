package view.ui;

import domain.events.start.StartSlideShowEvent;
import domain.events.update.SlideChangedEvent;
import domain.events.stop.StopSlideShowEvent;
import view.controllers.KeyController;
import view.controllers.MenuController;
import view.controllers.MouseController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * <p>Het applicatiewindow voor een slideviewcomponent</p>
 *
 * @author Ian F. Darwin, ian@darwinsys.com, Gert Florijn, Sylvia Stuurman
 * @version 1.6 2014/05/16 Sylvia Stuurman
 */

public class SlideViewerFrame extends JFrame implements ISlideShowViewComponent {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3460143382054168746L;
	public final static int WIDTH = 1200;
    public final static int HEIGHT = 800;

    private final SlideViewerComponent slideViewerComponent;
    private final KeyController keyController;
    private final MouseController mouseController;
    private final MenuController menuController;
    private final String title;

    public SlideViewerFrame(final String title,
                            final KeyController keyController,
                            final MenuController menuController
                            ) {
        super(title);
        this.title = title;
        this.keyController = keyController;
        this.menuController = menuController;
        this.slideViewerComponent = new SlideViewerComponent();
        this.mouseController = new MouseController(this.slideViewerComponent);

        this.setupWindow();
    }

    private void setupWindow() {
        final SlideViewerFrame currentFrame = this;
        this.add(this.slideViewerComponent);
        this.setTitle(title);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(final WindowEvent e) {
                currentFrame.slideShowStopped(new StopSlideShowEvent(this));
            }
        });
        this.getContentPane().add(slideViewerComponent);
        this.setMenuBar(menuController);
        this.addKeyListener(keyController);
        this.slideViewerComponent.addMouseListener(mouseController);
        this.setSize(new Dimension(WIDTH, HEIGHT));
        this.setVisible(true);
    }

    @Override
    public void slideShowStarted(final StartSlideShowEvent event) {
        this.slideViewerComponent.start(event.getSlide(), event.getPresentationSize());
    }

    @Override
    public void currentSlideChanged(final SlideChangedEvent event) {
        this.slideViewerComponent.update(event.getSlide());
    }

    @Override
    public void slideShowStopped(final StopSlideShowEvent event) {
        this.dispose();
        System.exit(0);
    }
}
