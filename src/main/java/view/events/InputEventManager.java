package view.events;

import domain.events.EventManager;
import domain.events.load.LoadSlideShowEventDispatcher;
import domain.events.navigateto.NavigateToSlideEventDispatcher;
import domain.events.save.SaveSlideShowEventDispatcher;
import domain.events.next.NextSlideEventDispatcher;
import domain.events.previous.PreviousSlideEventDispatcher;

public class InputEventManager extends EventManager {

    // Event dispatchers
    public final SaveSlideShowEventDispatcher saveSlideShowEventDispatcher;
    public final LoadSlideShowEventDispatcher loadSlideShowEventDispatcher;
    public final NextSlideEventDispatcher nextSlideEventDispatcher;
    public final PreviousSlideEventDispatcher previousSlideEventDispatcher;
    public final NavigateToSlideEventDispatcher navigateToEventDispatcher;

    public InputEventManager() {
        this.saveSlideShowEventDispatcher = new SaveSlideShowEventDispatcher();
        this.loadSlideShowEventDispatcher = new LoadSlideShowEventDispatcher();
        this.nextSlideEventDispatcher = new NextSlideEventDispatcher();
        this.previousSlideEventDispatcher = new PreviousSlideEventDispatcher();
        this.navigateToEventDispatcher = new NavigateToSlideEventDispatcher();
    }
}
