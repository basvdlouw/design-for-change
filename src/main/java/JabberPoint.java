import application.ApplicationController;
import view.ViewController;

public class JabberPoint {

    public static void main(final String[] argv) {
        final ApplicationController presentationController = new ApplicationController();
        new ViewController(presentationController);
        final String fileName = "actions_demonstration.xml";
        presentationController.start(fileName);
    }
}
