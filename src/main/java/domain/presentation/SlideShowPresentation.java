package domain.presentation;

import domain.builders.SimpleXMLBuilder;
import domain.builders.XMLBuilder;
import domain.builders.directors.SlideShowConstructor;
import domain.components.composite.slide.Slide;
import domain.components.composite.slideshow.SlideShow;
import domain.components.iterator.Iterator;
import domain.events.SlideShowEventManager;

public class SlideShowPresentation implements Presentation {

    private final SlideShowConstructor slideShowConstructor;
    private final XMLBuilder xmlBuilder;

    private SlideShow slideShow;
    private Iterator<Slide> slideIterator;

    private final SlideShowEventManager eventManager;

    public SlideShowPresentation(final SlideShowConstructor slideShowConstructor,
                                 final SimpleXMLBuilder simpleXMLBuilder,
                                 final SlideShowEventManager eventManager) {
        this.slideShowConstructor = slideShowConstructor;
        this.xmlBuilder = simpleXMLBuilder;
        this.eventManager = eventManager;
    }

    private void constructSlideShow() {
        this.slideShow = this.slideShowConstructor.constructSlideShow(this);
        this.slideIterator = this.slideShow.getIterator();
    }

    private Slide getCurrentSlide() {
        return this.slideIterator.currentItem();
    }

    @Override
    public void loadPresentation(final String filename) {
        this.slideShowConstructor.setXmlFile(filename);
        constructSlideShow();  // TODO: domain.presentation shouldn't be responsible for constructing a slideshow
    }

    @Override
    public void savePresentation(final String filename) {
        this.xmlBuilder.writeToFile(filename, slideShow);
    }

    @Override
    public void startPresentation() {
        if(this.slideShow == null)
            throw new IllegalArgumentException("Cannot start domain.presentation when no domain.presentation has been loaded");
        this.eventManager.startSlideShowEventDispatcher.slideShowStarted(this, this.getCurrentSlide(), this.getSize());
    }

    @Override
    public void stopPresentation() {
        this.eventManager.stopSlideShowEventDispatcher.slideShowStopped(this);
    }

    @Override
    public void nextSlide() {
        if (this.slideIterator.hasNext()) {
            this.slideIterator.next();
            this.eventManager.slideChangedEventDispatcher.slideChanged(this, this.getCurrentSlide());
        }
    }

    @Override
    public void previousSlide() {
        if (this.slideIterator.hasPrevious()) {
            this.slideIterator.previous();
            this.eventManager.slideChangedEventDispatcher.slideChanged(this, this.getCurrentSlide());
        }
    }

    @Override
    public void goToSlide(final int position) {
        this.slideIterator.goTo(position);
        this.eventManager.slideChangedEventDispatcher.slideChanged(this, this.getCurrentSlide());
    }

    @Override
    public int getSize() {
        return this.slideShow.getSize();
    }
}
