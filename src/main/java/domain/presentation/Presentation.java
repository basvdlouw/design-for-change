package domain.presentation;

public interface Presentation {

    void loadPresentation(final String filename);

    void savePresentation(final String filename);

    void startPresentation();

    void stopPresentation();

    void nextSlide();

    void previousSlide();

    void goToSlide(int position);

    int getSize();
}
