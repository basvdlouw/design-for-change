package domain.util;

import java.io.InputStream;

public final class FileLoader {
    public static InputStream loadFromResources(final String fileName) {
        return ClassLoader.getSystemResourceAsStream(fileName);
    }
}