package domain.builders.directors;

import domain.accessor.Accessor;
import domain.accessor.XMLAccessor;
import domain.builders.SlideShowBuilder;
import domain.components.composite.slide.SlideData;
import domain.components.composite.slideshow.SlideShow;
import domain.components.items.ItemData;
import domain.components.items.actions.ActionData;
import domain.components.items.figure.FigureData;
import domain.components.items.text.TextData;
import domain.presentation.Presentation;
import domain.util.FileLoader;

import java.io.InputStream;
import java.util.List;
import java.util.Objects;

public class XMLSlideShowConstructor implements SlideShowConstructor {

	private InputStream xmlFile;
	private final Accessor accessor;
	private final SlideShowBuilder slideShowBuilder;
	private Presentation presentation;

	/**
	 * Use XML Accessor if no domain.accessor provided
	 * @param slideShowBuilder
	 */
	public XMLSlideShowConstructor(final SlideShowBuilder slideShowBuilder) {
		this.accessor = new XMLAccessor();
		this.slideShowBuilder = slideShowBuilder;
	}

	@Override
	public SlideShow constructSlideShow(final Presentation presentation) {
		Objects.requireNonNull(this.xmlFile, "XMLFile expected to be non-null");
		this.presentation = presentation;
		this.parseXMLFile(this.xmlFile);
		this.buildSlideShow();
		this.buildSlides();
		this.buildItems((List<ItemData>) this.accessor.getItemsData());
		return this.slideShowBuilder.getSlideShow();
	}

	public void setXmlFile(String xmlFile) {
		this.xmlFile = FileLoader.loadFromResources(xmlFile);
	}

	private void parseXMLFile(final InputStream xmlFile) {
		this.accessor.loadFile(xmlFile);
	}

	private void buildSlideShow() {
		this.slideShowBuilder.buildSlideShow(this.accessor.getSlideShowData());
	}

	private void buildSlides() {
		this.accessor.getSlidesData().forEach(this::buildSlide);
	}

	private void buildSlide(final SlideData slideData) {
		this.slideShowBuilder.buildSlide(slideData);
	}

	private void buildItems(final List<ItemData> itemsData)
	{
		itemsData.forEach(itemData -> {
			switch (itemData.getItemType()) {
				case TEXT:
					this.slideShowBuilder.buildTextItem((TextData) itemData);
					break;
				case FIGURE:
					this.slideShowBuilder.buildFigureItem((FigureData) itemData);
					break;
				case ACTION:
					this.slideShowBuilder.buildAction((ActionData) itemData, this.presentation);
					break;
			}
		});
	}
}
