package domain.builders.directors;

import domain.components.composite.slideshow.SlideShow;
import domain.presentation.Presentation;

public interface SlideShowConstructor {
	SlideShow constructSlideShow(final Presentation presentation);
	void setXmlFile(String filename);
}
