package domain.builders;

import domain.components.ISlideShowComponent;
import domain.components.composite.slide.Slide;
import domain.components.composite.slideshow.SlideShow;
import domain.components.items.SlideItem;
import domain.components.items.figure.FigureItem;
import domain.components.items.text.TextItem;
import domain.components.iterator.Iterator;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class SimpleXMLBuilder implements XMLBuilder {

    private static final String DTD_FILE_CONTENTS = "<!ELEMENT showtitle (#PCDATA)>\n" +
            "<!ELEMENT domain.presentation (showtitle,slide+)>\n" +
            "<!ELEMENT title (#PCDATA)>\n" +
            "<!ELEMENT slide (title, item*)>\n" +
            "<!ELEMENT item (#PCDATA)>\n" +
            "<!ATTLIST item kind CDATA #REQUIRED>\n" +
            "<!ATTLIST item level CDATA #REQUIRED>\n";

    public void writeToFile(String filename, SlideShow simpleSlideShow) {
        PrintWriter out;
        try {
            out = new PrintWriter(new FileWriter(filename));
        } catch (IOException e) {
            throw new RuntimeException("Unable to write to file with filename: " + filename);
        }
        out.println("<?xml version=\"1.0\"?>");
        out.println("<!DOCTYPE domain.presentation SYSTEM \"jabberpoint.dtd\">");
        out.println("<domain.presentation>");
        out.print("<showtitle>");
        out.print(simpleSlideShow.getTitle());
        out.println("</showtitle>");

        Iterator<Slide> iterator = simpleSlideShow.getIterator();
        while(iterator.hasNext()) {
            Slide slide = iterator.currentItem();
            writeSlide(out, slide);
            iterator.next();
        }
        out.println("</domain.presentation>");
        out.close();
        writeXMLDTD(filename);
    }

    private void writeXMLDTD(String originalFilename) {
        String filename = originalFilename.replaceFirst(".xml", ".dtd");
        PrintWriter out;
        try {
            out = new PrintWriter(new FileWriter(filename));
        } catch (IOException e) {
            throw new RuntimeException("Unable to write to file with filename: " + filename);
        }

        out.print(DTD_FILE_CONTENTS);
        out.close();
    }

    private void writeSlide(PrintWriter out, Slide slide) {
        out.println("<slide>");
        out.println("<title>" + slide.getTitle() + "</title>");
        Iterator<ISlideShowComponent> slideIterator = slide.getIterator();

        while(slideIterator.hasNext()) {
            SlideItem slideItem = (SlideItem) slideIterator.currentItem();
            writeItem(out, slideItem);
            slideIterator.next();
        }
        out.println("</slide>");
    }

    private void writeItem(PrintWriter out, SlideItem slideItem) {
        out.print("<item kind=");
        if (slideItem instanceof TextItem) {
            out.print("\"text\" level=\"" + slideItem.getLevel().getLevel() + "\">");
            TextItem textItem = (TextItem) slideItem;
            out.print(textItem.getTextData().getText());
        } else {
            if (slideItem instanceof FigureItem) {
                out.print("\"image\" level=\"" + slideItem.getLevel().getLevel() + "\">");
                FigureItem figureItem = (FigureItem) slideItem;
                out.print(figureItem.getData().getImgUrl());
            } else {
                System.out.println("Ignoring " + slideItem);
            }
        }
        out.println("</item>");
    }
}
