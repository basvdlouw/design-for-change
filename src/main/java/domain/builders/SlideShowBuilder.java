package domain.builders;

import domain.components.composite.slide.SlideData;
import domain.components.composite.slideshow.SlideShow;
import domain.components.composite.slideshow.SlideShowData;
import domain.components.items.actions.ActionData;
import domain.components.items.figure.FigureData;
import domain.components.items.text.TextData;
import domain.presentation.Presentation;

public interface SlideShowBuilder {

    SlideShow getSlideShow();

    void buildSlideShow(SlideShowData data);

    void buildSlide(SlideData data);

    void buildTextItem(TextData data);

    void buildFigureItem(FigureData data);

    void buildAction(ActionData data, Presentation presentation);
}
