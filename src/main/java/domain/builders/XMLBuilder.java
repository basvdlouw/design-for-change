package domain.builders;

import domain.components.composite.slideshow.SlideShow;

public interface XMLBuilder {
    void writeToFile(String filename, SlideShow simpleSlideShow);
}
