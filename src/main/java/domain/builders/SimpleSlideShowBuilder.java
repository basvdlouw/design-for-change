package domain.builders;

import domain.components.composite.slide.SimpleSlide;
import domain.components.composite.slide.Slide;
import domain.components.composite.slide.SlideData;
import domain.components.composite.slideshow.SimpleSlideShow;
import domain.components.composite.slideshow.SlideShow;
import domain.components.composite.slideshow.SlideShowData;
import domain.components.items.ISlideItem;
import domain.components.items.ItemData;
import domain.components.items.actions.*;
import domain.components.items.actions.blackboard.ActionDataIntValue;
import domain.components.items.actions.blackboard.ActionDataStringValue;
import domain.components.items.actions.blackboard.ActionDataValue;
import domain.components.items.figure.FigureData;
import domain.components.items.figure.FigureItem;
import domain.components.items.style.Level;
import domain.components.items.text.TextData;
import domain.components.items.text.TextItem;
import domain.presentation.Presentation;

import java.util.ArrayList;
import java.util.List;

public class SimpleSlideShowBuilder implements SlideShowBuilder {

    private SimpleSlideShow slideShow;
    private final List<Slide> slides;

    public SimpleSlideShowBuilder() {
        this.slides = new ArrayList<>();
    }

    private Level constructLevel(final int level) {
        return new Level(level);
    }

    private void addItemToSlide(final ISlideItem slideItem, final int sequenceNumber) {
        this.slides.stream().filter(slide -> slide.getSequenceNumber() == sequenceNumber).forEach(slide -> slide.add(slideItem));
    }

    @Override
    public void buildSlideShow(final SlideShowData slideShowData) {
        this.slideShow = new SimpleSlideShow(slideShowData);
        this.slides.clear();
    }

    @Override
    public void buildSlide(final SlideData slideData) {
        final SimpleSlide slide = new SimpleSlide(slideData);
        this.slides.add(slide);
    }

    @Override
    public void buildTextItem(final TextData data) {
        final ISlideItem textItem = createTextItem(data);
        this.addItemToSlide(textItem, data.getSlideSequenceNumber());
    }

    private ISlideItem createTextItem(final TextData data) {
        final Level level = this.constructLevel(data.getLevel());
        return new TextItem(level, data);
    }

    @Override
    public void buildFigureItem(final FigureData data) {
        final ISlideItem figureItem = createFigureItem(data);
        this.addItemToSlide(figureItem, data.getSlideSequenceNumber());
    }

    private ISlideItem createFigureItem(final FigureData data) {
        final Level level = this.constructLevel(data.getLevel());
        return new FigureItem(level, data);
    }

    @Override
    public void buildAction(final ActionData data, final Presentation presentation) {
        final ISlideItem actionItem = createAction(data, presentation);
        this.addItemToSlide(actionItem, data.getSlideSequenceNumber());
    }

    private ISlideItem createAction(final ActionData data, final Presentation presentation) {
        ISlideItem finalItem = null;

        for (int i = 0; i < data.getData().size(); i++) {
            final ItemData itemData = data.getData().get(i);
            finalItem = createItem(itemData, presentation);
        }

        finalItem = createAction(finalItem, data, presentation);

        return finalItem;
    }

    private ISlideItem createItem(final ItemData itemData, final Presentation presentation) {
        switch (itemData.getItemType()) {
            case TEXT:
                return createTextItem((TextData) itemData);
            case FIGURE:
                return createFigureItem((FigureData) itemData);
            case ACTION:
                return createAction((ActionData) itemData, presentation);
            default:
                throw new IllegalArgumentException();
        }
    }

    private ISlideItem createAction(final ISlideItem item, final ActionData data, final Presentation presentation) {
        final ActionDataValue<?> actionDataValue = data.getValue("value");

        switch (data.getActionType()) {
            case BEEP:
                return new BeepActionDecorator(item, presentation);
            case NEXT:
                return new NextSlideActionDecorator(item, presentation);
            case PREV:
                return new PreviousSlideActionDecorator(item, presentation);
            case GOTO:
                return new GoToSlideActionDecorator(item, (ActionDataIntValue) actionDataValue, presentation);
            case OPEN:
                return new OpenPresentationActionDecorator(item, (ActionDataStringValue) actionDataValue, presentation);
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public SlideShow getSlideShow() {
        slideShow.setComponents(this.slides);
        return this.slideShow;
    }
}
