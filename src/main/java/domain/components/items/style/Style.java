package domain.components.items.style;

import java.awt.*;

/**
 * <p>Style staat voor Indent, Color, Font and Leading.</p>
 * <p>De koppeling tussen style-nummer en item-level is nu direct:
 * in Slide wordt de style opgehaald voor een item
 * met als style-nummer het item-level.</p>
 *
 * @author Ian F. Darwin, ian@darwinsys.com, Gert Florijn, Sylvia Stuurman
 * @version 1.6 2014/05/16 Sylvia Stuurman
 */

public class Style {

    // De styles zijn vast ingecodeerd.
    private static final Style[] STYLES = new Style[]{
            new Style(0, Color.red, 48, 20),  // style voor item-level 0
            new Style(20, Color.blue, 40, 10),   // style voor item-level 1
            new Style(50, Color.black, 36, 10),   // style voor item-level 2
            new Style(70, Color.black, 30, 10),  // style voor item-level 3
            new Style(90, Color.black, 24, 10)    // style voor item-level 4
    };

    private static final String FONT_NAME = "Helvetica";

    private final int indent;
    private final Color color;
    private final Font font;
    private final int fontSize;
    private final int leading;

    public static Style getStyle(int level) {
        if (level >= STYLES.length) {
            level = STYLES.length - 1;
        }
        return STYLES[level];
    }

    public Style(int indent, Color color, int points, int leading) {
        this.indent = indent;
        this.color = color;
        font = new Font(FONT_NAME, Font.BOLD, fontSize = points);
        this.leading = leading;
    }

    public String toString() {
        return "[" + indent + "," + color + "; " + fontSize + " on " + leading + "]";
    }

    public Font getFont(float scale) {
        return font.deriveFont(fontSize * scale);
    }

    public int getIndent() {
        return indent;
    }

    public Color getColor() {
        return color;
    }

    public Font getFont() {
        return font;
    }

    public int getFontSize() {
        return fontSize;
    }

    public int getLeading() {
        return leading;
    }
}
