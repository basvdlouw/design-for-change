package domain.components.items.style;

public class Level {
    
    private final int level;

    public Level(final int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }
}
