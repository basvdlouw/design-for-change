package domain.components.items.text;

import domain.components.items.SlideItem;
import domain.components.items.style.Level;
import domain.components.visit.visitor.Visitor;

public class TextItem extends SlideItem {

    private final TextData textData;

    public TextItem(final Level level, final TextData textData) {
        super(level);
        this.textData = textData;
    }

    public TextData getTextData() {
        return textData;
    }

    @Override
    public void accept(final Visitor visitor) {
        visitor.visit(this);
    }
}
