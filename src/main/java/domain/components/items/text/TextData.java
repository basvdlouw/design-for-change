package domain.components.items.text;

import domain.components.items.ItemData;
import domain.components.items.ItemType;

public class TextData extends ItemData {

    private final String text;

    public TextData(final String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @Override
    public ItemType getItemType() {
        return ItemType.TEXT;
    }
}
