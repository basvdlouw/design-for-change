package domain.components.items.figure;

import domain.components.items.ItemData;
import domain.components.items.ItemType;

public class FigureData extends ItemData {

    private final String imgUrl;

    public FigureData(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    @Override
    public ItemType getItemType() {
        return ItemType.FIGURE;
    }
}
