package domain.components.items.figure;

import domain.components.items.SlideItem;
import domain.components.items.style.Level;
import domain.components.visit.visitor.Visitor;
import domain.util.FileLoader;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class FigureItem extends SlideItem {

    private final FigureData figureData;

    public FigureItem(final Level level, final FigureData figureData) {
        super(level);
        this.figureData = figureData;
    }

    public BufferedImage getFigure() {
        BufferedImage figure = null;
        try {
            figure = ImageIO.read(FileLoader.loadFromResources(this.figureData.getImgUrl()));
        } catch (final IOException e) {
            e.printStackTrace();
        }
        return figure;
    }

    public FigureData getData() {
        return figureData;
    }

    @Override
    public void accept(final Visitor visitor) {
        visitor.visit(this);
    }
}
