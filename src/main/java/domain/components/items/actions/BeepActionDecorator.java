package domain.components.items.actions;

import domain.components.items.ISlideItem;
import domain.components.items.SlideItemActionDecorator;
import domain.presentation.Presentation;

import java.awt.*;

public class BeepActionDecorator extends SlideItemActionDecorator {

    public BeepActionDecorator(final ISlideItem slideItem, final Presentation presentation) {
        super(slideItem, presentation);
    }

    @Override
    public void onAction() {
        Toolkit.getDefaultToolkit().beep();
        super.onAction();
    }
}
