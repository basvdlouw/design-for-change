package domain.components.items.actions.blackboard;

public abstract class ActionDataValue<T> {
    protected T value;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public abstract void parse(String value);
}
