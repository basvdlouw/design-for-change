package domain.components.items.actions.blackboard;

public class ActionDataStringValue extends ActionDataValue<String> {
    public ActionDataStringValue() {}
    public ActionDataStringValue(String value) {
        this.value = value;
    }

    @Override
    public void parse(String value) {
        setValue(value);
    }
}
