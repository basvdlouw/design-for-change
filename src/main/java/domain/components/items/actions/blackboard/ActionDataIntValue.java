package domain.components.items.actions.blackboard;

public class ActionDataIntValue extends ActionDataValue<Integer> {
    public ActionDataIntValue() {}
    public ActionDataIntValue(int value) {
        this.value = value;
    }

    @Override
    public void parse(String value) {
        int parsedValue = 0;
        try {
            parsedValue = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new NumberFormatException();
        }

        setValue(parsedValue);
    }
}
