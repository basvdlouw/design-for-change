package domain.components.items.actions;

import domain.components.items.ISlideItem;
import domain.components.items.SlideItemActionDecorator;
import domain.components.items.actions.blackboard.ActionDataStringValue;
import domain.presentation.Presentation;

public class OpenPresentationActionDecorator extends SlideItemActionDecorator {
    private final String targetPath;

    public OpenPresentationActionDecorator(final ISlideItem slideItem, final ActionDataStringValue targetPath, final Presentation presentation) {
        super(slideItem, presentation);

        if (targetPath != null) {
            this.targetPath = targetPath.getValue();
        }
        else {
            this.targetPath = "";
        }
    }

    @Override
    public void onAction() {
        presentation.loadPresentation(targetPath);
        presentation.startPresentation();
        super.onAction();
    }
}
