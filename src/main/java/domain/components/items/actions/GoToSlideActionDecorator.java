package domain.components.items.actions;

import domain.components.items.ISlideItem;
import domain.components.items.SlideItemActionDecorator;
import domain.components.items.actions.blackboard.ActionDataIntValue;
import domain.presentation.Presentation;

public class GoToSlideActionDecorator extends SlideItemActionDecorator {
    private final int targetSlide;

    public GoToSlideActionDecorator(final ISlideItem slideItem, final ActionDataIntValue targetSlide, final Presentation presentation) {
        super(slideItem, presentation);

        if (targetSlide != null) {
            this.targetSlide = targetSlide.getValue();
        }
        else {
            this.targetSlide = 0;
        }
    }

    @Override
    public void onAction() {
        this.presentation.goToSlide(this.targetSlide);
        super.onAction();
    }
}
