package domain.components.items.actions;

import domain.components.items.ISlideItem;
import domain.components.items.SlideItemActionDecorator;
import domain.presentation.Presentation;

public class NextSlideActionDecorator extends SlideItemActionDecorator {

    public NextSlideActionDecorator(final ISlideItem slideItem, final Presentation presentation) {
        super(slideItem, presentation);
    }

    @Override
    public void onAction() {
        this.presentation.nextSlide();
        super.onAction();
    }
}
