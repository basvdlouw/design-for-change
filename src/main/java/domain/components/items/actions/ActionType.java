package domain.components.items.actions;

public enum ActionType {
    BEEP,
    NEXT,
    PREV,
    GOTO,
    OPEN
}
