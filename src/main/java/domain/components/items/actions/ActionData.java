package domain.components.items.actions;

import domain.components.items.ItemData;
import domain.components.items.ItemType;
import domain.components.items.actions.blackboard.ActionDataValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: currently has useless info from parent ItemData, split off somehow?
public class ActionData extends ItemData
{
    private final List<ItemData> childData;
    private final ActionType actionType;
    private final Map<String, ActionDataValue<?>> actionDataValues;

    public ActionData(final ActionType actionType) {
        this.childData = new ArrayList<>();
        this.actionDataValues = new HashMap<>();
        this.actionType = actionType;
    }

    public List<ItemData> getData() {
        return childData;
    }

    public void addItems(final List<ItemData> items) {
        childData.addAll(items);
    }

    public void setValue(final String key, final ActionDataValue<?> value) {
        actionDataValues.put(key, value);
    }

    public ActionDataValue<?> getValue(final String key) {
        return actionDataValues.getOrDefault(key, null);
    }

    public ActionType getActionType() {
        return actionType;
    }

    @Override
    public ItemType getItemType() {
        return ItemType.ACTION;
    }
}
