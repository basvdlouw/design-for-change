package domain.components.items;

import domain.components.SlideShowComponent;
import domain.components.items.style.Level;

public abstract class SlideItem extends SlideShowComponent implements ISlideItem {
    protected final Level level;

    protected SlideItem(final Level level) {
        this.level = level;
    }

    @Override
    public Level getLevel() {
        return level;
    }
}
