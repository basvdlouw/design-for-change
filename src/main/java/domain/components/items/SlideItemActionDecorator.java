package domain.components.items;

import domain.components.SlideShowComponent;
import domain.components.items.style.Level;
import domain.components.visit.visitor.Visitor;
import domain.presentation.Presentation;

public abstract class SlideItemActionDecorator extends SlideShowComponent implements ISlideItem {

    /**
     * Action items hold a reference to domain.presentation so that they can execute actions on the domain.presentation.
     */
    protected final Presentation presentation;
    protected final ISlideItem slideItem;

    protected SlideItemActionDecorator(final ISlideItem slideItem, final Presentation presentation) {
        this.slideItem = slideItem;
        this.presentation = presentation;
    }

    @Override
    public Level getLevel() {
        return this.slideItem.getLevel();
    }

    public void onAction() {
        if (this.slideItem instanceof SlideItemActionDecorator) {
            ((SlideItemActionDecorator) this.slideItem).onAction();
        }
    }

    public ISlideItem getSlideItem() {
        return slideItem;
    }

    @Override
    public void accept(final Visitor visitor) {
        visitor.visit(this);
    }
}
