package domain.components.items;

public abstract class ItemData extends Data {

    private int level;
    private int slideSequenceNumber;

    public abstract ItemType getItemType();

    public int getLevel() {
        return level;
    }

    public void setLevel(final int level) {
        this.level = level;
    }

    public int getSlideSequenceNumber() {
        return slideSequenceNumber;
    }

    public void setSlideSequenceNumber(final int slideSequenceNumber) {
        this.slideSequenceNumber = slideSequenceNumber;
    }
}
