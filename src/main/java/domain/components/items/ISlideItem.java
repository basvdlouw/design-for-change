package domain.components.items;

import domain.components.ISlideShowComponent;
import domain.components.items.style.Level;

public interface ISlideItem extends ISlideShowComponent {

    /**
     * Stylistic properties can be received using this method
     *
     * @return level
     */
    Level getLevel();
}
