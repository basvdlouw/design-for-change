package domain.components.items;

public enum ItemType {
    TEXT,
    FIGURE,
    ACTION
}
