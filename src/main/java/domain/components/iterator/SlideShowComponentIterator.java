package domain.components.iterator;

import domain.components.ISlideShowComponent;
import domain.components.composite.ISlideShowComposite;


/**
 * Generic Iterator for SlideShowComposites
 */
public final class SlideShowComponentIterator<T extends ISlideShowComponent> implements Iterator<T> {

    private final ISlideShowComposite<T> slideShowComposite;

    private int index;

    public SlideShowComponentIterator(final ISlideShowComposite<T> slideShowComposite) {
        this.slideShowComposite = slideShowComposite;
    }

    @Override
    public void next() {
        this.index++;
    }

    @Override
    public void previous() {
        this.index--;
    }

    @Override
    public boolean hasNext() {
        return this.index < this.slideShowComposite.getComponents().size() - 1;
    }

    @Override
    public boolean hasPrevious() {
        return this.index > 0;
    }

    @Override
    public T currentItem() {
        return this.slideShowComposite.getComponents().get(index);
    }

    @Override
    public void goTo(int position) {
        if (position == index) {
            return;
        }
        if(position > index) {
            while(hasNext() && position > index) {
                next();
            }
        } else {
            while(hasPrevious() && position < index) {
                previous();
            }
        }
    }
}
