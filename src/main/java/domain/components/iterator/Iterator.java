package domain.components.iterator;


public interface Iterator<E> {

	/**
	 * Moves iterator index to the next item in the collection
	 */
	void next();

	/**
	 * Moves iterator index to the previous item in the collection
	 */
	void previous();

	/**
	 * Returns true if the iterator has more elements when traversing the list in the forward direction.
	 * @return boolean
	 */
	boolean hasNext();

	/**
	 * Returns true if the iterator has more elements when traversing the list in the reverse direction.
	 * @return boolean
	 */
	boolean hasPrevious();

	/**
	 * Returns item on the iterator current index
	 * @return E
	 */
	E currentItem();

	/**
	 * Navigate to item on given index.
	 * @param position
	 */
	void goTo(int position);

}
