package domain.components.visit.visitable;

import domain.components.visit.visitor.Visitor;

public interface Visitable {
    void accept(final Visitor visitor);
}
