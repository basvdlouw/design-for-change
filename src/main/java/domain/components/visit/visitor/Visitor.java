package domain.components.visit.visitor;

import domain.components.ISlideShowComponent;
import domain.components.composite.ISlideShowComposite;
import domain.components.items.SlideItemActionDecorator;
import domain.components.items.figure.FigureItem;
import domain.components.items.text.TextItem;

public interface Visitor {

    void visit(ISlideShowComposite<? extends ISlideShowComponent> composite);

    void visit(TextItem textItem);

    void visit(SlideItemActionDecorator slideItemActionDecorator);

    void visit(FigureItem figureItem);
}




