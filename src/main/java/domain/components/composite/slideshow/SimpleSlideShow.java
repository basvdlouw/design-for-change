package domain.components.composite.slideshow;

import domain.components.composite.SlideShowComposite;
import domain.components.composite.slide.Slide;

public class SimpleSlideShow extends SlideShowComposite<Slide> implements SlideShow {

    private final SlideShowData slideShowData;

    public SimpleSlideShow(final SlideShowData slideShowData) {
        this.slideShowData = slideShowData;
    }

    public String getTitle() {
        return this.slideShowData.getTitle();
    }

    @Override
    public int getSize() {
        return this.components.size();
    }
}
