package domain.components.composite.slideshow;

import domain.components.items.Data;

public class SlideShowData extends Data {

    private String title;

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
