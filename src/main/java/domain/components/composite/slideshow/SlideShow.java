package domain.components.composite.slideshow;

import domain.components.composite.ISlideShowComposite;
import domain.components.composite.slide.Slide;
import domain.components.iterator.Iterator;

public interface SlideShow extends ISlideShowComposite<Slide> {

    String getTitle();

    int getSize();

    Iterator<Slide> getIterator();
}
