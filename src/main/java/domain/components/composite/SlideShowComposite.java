package domain.components.composite;

import domain.components.ISlideShowComponent;
import domain.components.SlideShowComponent;
import domain.components.iterator.Iterator;
import domain.components.iterator.SlideShowComponentIterator;
import domain.components.visit.visitor.Visitor;

import java.util.ArrayList;
import java.util.List;

public abstract class SlideShowComposite<T extends ISlideShowComponent> extends SlideShowComponent implements ISlideShowComposite<T> {

    protected final Iterator<T> iterator;
    protected List<T> components;

    protected SlideShowComposite() {
        this.components = new ArrayList<>();
        this.iterator = new SlideShowComponentIterator<>(this);
    }

    @Override
    public void add(final T slideShowComponent) {
        this.components.add(slideShowComponent);
    }

    @Override
    public void remove(final T slideShowComponent) {
        this.components.remove(slideShowComponent);
    }

    @Override
    public List<T> getComponents() {
        return this.components;
    }

    public void setComponents(final List<T> components) {
        this.components = components;
    }

    @Override
    public Iterator<T> getIterator() {
        return iterator;
    }

    @Override
    public void accept(final Visitor visitor) {
        visitor.visit(this);
    }
}
