package domain.components.composite;

import domain.components.ISlideShowComponent;
import domain.components.iterator.Iterator;

import java.util.List;

public interface ISlideShowComposite<T extends ISlideShowComponent> extends ISlideShowComponent {

    void add(final T slideShowComponent);

    void remove(final T slideShowComponent);

    List<T> getComponents();

    Iterator<T> getIterator();
}
