package domain.components.composite.slide;

import domain.components.items.Data;

public class SlideData extends Data {
    private int sequenceNumber;
    private String title;

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }
}
