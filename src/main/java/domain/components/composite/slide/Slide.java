package domain.components.composite.slide;

import domain.components.ISlideShowComponent;
import domain.components.composite.ISlideShowComposite;

public interface Slide extends ISlideShowComposite<ISlideShowComponent> {

    /**
     * @return slide title
     */
    String getTitle();

    /**
     * Every slide has a sequence number
     * @return sequenceNumber
     */
    int getSequenceNumber();
}