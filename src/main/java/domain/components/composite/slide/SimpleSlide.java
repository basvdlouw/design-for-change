package domain.components.composite.slide;

import domain.components.ISlideShowComponent;
import domain.components.composite.SlideShowComposite;
import domain.components.items.ISlideItem;
import domain.components.items.style.Level;
import domain.components.items.text.TextData;
import domain.components.items.text.TextItem;

public class SimpleSlide extends SlideShowComposite<ISlideShowComponent> implements Slide {

    private final SlideData slideData;

    public SimpleSlide(final SlideData slideData) {
        this.slideData = slideData;
        // Slide title
        final ISlideItem slideTitleItem = new TextItem(new Level(0), new TextData(this.slideData.getTitle()));
        this.components.add(slideTitleItem);
    }

    @Override
    public int getSequenceNumber() {
        return this.slideData.getSequenceNumber();
    }

    @Override
    public String getTitle() {
        return this.slideData.getTitle();
    }
}
