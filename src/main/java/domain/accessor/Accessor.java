package domain.accessor;

import domain.components.composite.slide.SlideData;
import domain.components.composite.slideshow.SlideShowData;
import domain.components.items.ItemData;

import java.io.InputStream;

/**
 * <p>Een Accessor maakt het mogelijk om gegevens voor een presentatie
 * te lezen of te schrijven naar een bestand.</p>
 *
 * @author Ian F. Darwin, ian@darwinsys.com, Gert Florijn, Sylvia Stuurman
 * @version 1.6 2014/05/16 Sylvia Stuurman
 */

public interface Accessor {

    void loadFile(final InputStream file);

    void saveFile(final InputStream file);

    SlideShowData getSlideShowData();

    Iterable<SlideData> getSlidesData();

    Iterable<ItemData> getItemsData();
}
