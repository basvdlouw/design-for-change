package domain.accessor;

import domain.components.composite.slide.SlideData;
import domain.components.composite.slideshow.SlideShowData;
import domain.components.items.ItemData;
import domain.components.items.ItemType;
import domain.components.items.actions.ActionData;
import domain.components.items.actions.ActionType;
import domain.components.items.actions.blackboard.ActionDataIntValue;
import domain.components.items.actions.blackboard.ActionDataStringValue;
import domain.components.items.actions.blackboard.ActionDataValue;
import domain.components.items.figure.FigureData;
import domain.components.items.text.TextData;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class XMLAccessor implements Accessor {

    private Document xmlDocument;
    private final int DEFAULT_LEVEL = 1;
    /**
     * Names of XML tag attributes
     */
    protected static final String SLIDESHOW_TITLE = "showtitle";
    protected static final String SLIDE_TITLE = "title";
    protected static final String SLIDE = "slide";
    protected static final String ITEMS = "items";
    protected static final String LEVEL = "level";
    protected static final String TEXT = "text";
    protected static final String FIGURE = "image";
    protected static final String ACTION = "action";
    protected static final String ACTIONTYPE = "name";
    protected static final String ACTIONTYPE_BEEP = "beep";
    protected static final String ACTIONTYPE_NEXT = "next";
    protected static final String ACTIONTYPE_PREV = "previous";
    protected static final String ACTIONTYPE_GOTO = "goto";
    protected static final String ACTIONTYPE_OPEN = "open";

    private void initializeDocument(final InputStream xmlFile) {
        try {
            this.xmlDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlFile);
        } catch (final SAXException | IOException | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    private Element getDocumentElement() {
        return this.xmlDocument.getDocumentElement();
    }

    private String getTitle(final Element element, final String tagName) {
        final NodeList titles = element.getElementsByTagName(tagName);
        return titles.item(0).getTextContent();
    }

    private int parseLevelAttribute(final String levelStr) {
        int level = this.DEFAULT_LEVEL;

        if (levelStr != null && !levelStr.isEmpty()) {
            try {
                level = Integer.parseInt(levelStr);
            } catch (final NumberFormatException ex) {
                ex.printStackTrace();
            }
        }

        return level;
    }

    private ItemType parseItemTypeAttribute(final String itemType) {
        //noinspection EnhancedSwitchMigration (old switch case to support backwards compatibility)
        switch (itemType) {
            case TEXT:
                return ItemType.TEXT;
            case FIGURE:
                return ItemType.FIGURE;
            case ACTION:
                return ItemType.ACTION;
            default:
                throw new IllegalArgumentException(String.format("Can not compute item: '%s', TYPE UNKNOWN", itemType));
        }
    }

    private ActionType parseActionTypeAttribute(final String actionType) {
        switch (actionType) {
            case ACTIONTYPE_BEEP:
                return ActionType.BEEP;
            case ACTIONTYPE_NEXT:
                return ActionType.NEXT;
            case ACTIONTYPE_PREV:
                return ActionType.PREV;
            case ACTIONTYPE_GOTO:
                return ActionType.GOTO;
            case ACTIONTYPE_OPEN:
                return ActionType.OPEN;
            default:
                throw new IllegalArgumentException(String.format("Can not compute action: '%s', TYPE UNKNOWN", actionType));
        }
    }

    private ItemData setItemData(final Element item, final ItemType itemType) {
        final ItemData itemData;
        switch (itemType) {
            case TEXT:
                itemData = new TextData(item.getTextContent());
                break;
            case FIGURE:
                itemData = new FigureData(item.getTextContent());
                break;
            case ACTION:
                itemData = new ActionData(parseActionTypeAttribute(item.getAttribute(ACTIONTYPE)));
                break;
            default:
                throw new IllegalArgumentException();
        }
        itemData.setLevel(parseLevelAttribute(item.getAttribute(LEVEL)));
        return itemData;
    }

    @Override
    public SlideShowData getSlideShowData() {
        final SlideShowData slideShowData = new SlideShowData();
        slideShowData.setTitle(getTitle(this.getDocumentElement(), SLIDESHOW_TITLE));
        return slideShowData;
    }

    @Override
    public List<SlideData> getSlidesData() {
        final List<SlideData> slideDataList = new ArrayList<>();
        final NodeList slideNodes = this.getDocumentElement().getElementsByTagName(SLIDE);
        for (int i = 0; i < slideNodes.getLength(); i++) {
            final Element xmlSlide = (Element) slideNodes.item(i);
            final SlideData slideData = new SlideData();
            slideData.setSequenceNumber(i + 1);
            slideData.setTitle(getTitle(xmlSlide, SLIDE_TITLE));
            slideDataList.add(slideData);
        }
        return slideDataList;
    }

    @Override
    public List<ItemData> getItemsData() {
        final List<ItemData> itemDataList = new ArrayList<>();
        final NodeList slideNodes = this.getDocumentElement().getElementsByTagName(ITEMS);

        for (int i = 0; i < slideNodes.getLength(); i++) // i == slide sequence number
        {
            final Element slideElement = (Element) slideNodes.item(i);
            itemDataList.addAll(getItemChildrenData(slideElement, i + 1));
        }

        return itemDataList;
    }

    public List<ItemData> getItemChildrenData(final Element parentElement, final int slideSeqNum) {
        final NodeList slideItems = parentElement.getChildNodes();
        final List<ItemData> childItems = new ArrayList<>();

        for (int i = 0; i < slideItems.getLength(); i++) {
            final Node childItem = slideItems.item(i);
            if (childItem instanceof Element) {
                final Element childElement = (Element) childItem;
                final ItemType itemType = parseItemTypeAttribute(childElement.getTagName());

                // Create item, set sequence number
                final ItemData itemData = this.setItemData(childElement, itemType);
                itemData.setSlideSequenceNumber(slideSeqNum);

                // If it's an action, add values and children to action
                if (itemType == ItemType.ACTION && childElement.hasChildNodes()) {
                    final ActionData actionData = (ActionData) itemData;
                    addActionParamsToData(childElement, actionData);
                    actionData.addItems(getItemChildrenData(childElement, slideSeqNum));
                }

                childItems.add(itemData);
            }
        }

        return childItems;
    }

    private void addActionParamsToData(final Element actionElement, final ActionData actionData) {
        final NamedNodeMap attributes = actionElement.getAttributes();
        ActionDataValue<?> actionDataValue = null;
        String value = "";

        // Get the required data regardless of attribute order
        for (int i = 0; i < attributes.getLength(); i++) {
            final String attributeName  = attributes.item(i).getNodeName();
            final String attributeValue = attributes.item(i).getNodeValue();

            switch (attributeName) {
                case "valuetype":
                    actionDataValue = getActionDataValue(attributeValue);
                    break;
                case "value":
                    value = attributeValue;
                    break;
            }
        }

        // Assemble the data into ActionDataValue
        if (actionDataValue != null) {
            actionDataValue.parse(value);
            actionData.setValue("value", actionDataValue);
        }
    }

    private ActionDataValue<?> getActionDataValue(final String attributeValue) {
        switch (attributeValue) {
            case "string":
                return new ActionDataStringValue();
            case "int":
                return new ActionDataIntValue();
        }
        return new ActionDataStringValue();
    }

    @Override
    public void loadFile(final InputStream file) {
        this.initializeDocument(file);
    }

    @Override
    public void saveFile(final InputStream file) {
        throw new UnsupportedOperationException();
    }
}
