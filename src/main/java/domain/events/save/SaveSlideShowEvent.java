package domain.events.save;

import java.util.EventObject;

public class SaveSlideShowEvent extends EventObject {
	private static final long serialVersionUID = -312996858571903876L;
	private final String filename;

    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public SaveSlideShowEvent(final Object source, final String filename) {
        super(source);
        this.filename = filename;
    }

    public String getFilename() {
        return this.filename;
    }
}
