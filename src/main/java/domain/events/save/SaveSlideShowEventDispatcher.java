package domain.events.save;

import domain.events.EventDispatcher;

public class SaveSlideShowEventDispatcher extends EventDispatcher<SaveSlideShowEventListener, SaveSlideShowEvent> {

    public void saveSlideShow(final Object source, final String filename) {
        final SaveSlideShowEvent event = new SaveSlideShowEvent(source, filename);
        this.fireEvent(event);
    }

    @Override
    protected void fireEvent(final SaveSlideShowEvent saveSlideShowEvent) {
        this.eventListeners.forEach(eventListener -> eventListener.saveSlideShow(saveSlideShowEvent));
    }
}
