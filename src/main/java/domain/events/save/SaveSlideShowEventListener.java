package domain.events.save;

import java.util.EventListener;

public interface SaveSlideShowEventListener extends EventListener {
    void saveSlideShow(SaveSlideShowEvent event);
}
