package domain.events.start;

import domain.components.composite.slide.Slide;
import domain.events.EventDispatcher;

public class StartSlideShowEventDispatcher extends EventDispatcher<StartSlideShowEventListener, StartSlideShowEvent> {

    public void slideShowStarted(final Object source, final Slide slide, final int presentationSize) {
        final StartSlideShowEvent event = new StartSlideShowEvent(source, slide, presentationSize);
        this.fireEvent(event);
    }

    @Override
    protected void fireEvent(final StartSlideShowEvent startSlideShowEvent) {
        this.eventListeners.forEach(eventListener -> eventListener.slideShowStarted(startSlideShowEvent));
    }
}
