package domain.events.start;

import java.util.EventListener;

public interface StartSlideShowEventListener extends EventListener {
    void slideShowStarted(StartSlideShowEvent event);
}
