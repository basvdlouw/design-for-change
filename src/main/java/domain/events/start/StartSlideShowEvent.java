package domain.events.start;

import domain.components.composite.slide.Slide;

import java.util.EventObject;

public class StartSlideShowEvent extends EventObject {

	private static final long serialVersionUID = -9065293905308080811L;

	private final int presentationSize;

    private final Slide slide;

    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public StartSlideShowEvent(final Object source, final Slide slide, final int presentationSize) {
        super(source);
        this.slide = slide;
        this.presentationSize = presentationSize;
    }

    public Slide getSlide() {
        return this.slide;
    }

    public int getPresentationSize() {
        return presentationSize;
    }

}
