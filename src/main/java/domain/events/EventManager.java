package domain.events;

import domain.events.start.StartSlideShowEventDispatcher;
import domain.events.stop.StopSlideShowEventDispatcher;

public abstract class EventManager {

    public final StartSlideShowEventDispatcher startSlideShowEventDispatcher;
    public final StopSlideShowEventDispatcher stopSlideShowEventDispatcher;

    protected EventManager() {
        this.startSlideShowEventDispatcher = new StartSlideShowEventDispatcher();
        this.stopSlideShowEventDispatcher = new StopSlideShowEventDispatcher();
    }
}
