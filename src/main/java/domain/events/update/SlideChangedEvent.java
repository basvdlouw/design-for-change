package domain.events.update;

import domain.components.composite.slide.Slide;

import java.util.EventObject;

public class SlideChangedEvent extends EventObject {
	private static final long serialVersionUID = -4284417131599497676L;
	private final Slide slide;

    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public SlideChangedEvent(final Object source, final Slide slide) {
        super(source);
        this.slide = slide;
    }

    public Slide getSlide() {
        return this.slide;
    }
}
