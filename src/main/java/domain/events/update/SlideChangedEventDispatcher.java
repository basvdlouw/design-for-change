package domain.events.update;


import domain.components.composite.slide.Slide;
import domain.events.EventDispatcher;

public class SlideChangedEventDispatcher extends EventDispatcher<SlideChangedEventListener, SlideChangedEvent> {

    public void slideChanged(final Object source, final Slide slide) {
        final SlideChangedEvent event = new SlideChangedEvent(source, slide);
        this.fireEvent(event);
    }

    @Override
    protected void fireEvent(final SlideChangedEvent slideChangedEvent) {
        eventListeners.forEach(eventListener -> eventListener.currentSlideChanged(slideChangedEvent));
    }
}
