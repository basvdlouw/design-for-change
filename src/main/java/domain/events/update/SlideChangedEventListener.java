package domain.events.update;

import java.util.EventListener;

public interface SlideChangedEventListener extends EventListener {
    void currentSlideChanged(SlideChangedEvent event);
}
