package domain.events.previous;

import domain.events.EventDispatcher;

public class PreviousSlideEventDispatcher extends EventDispatcher<PreviousSlideEventListener, PreviousSlideEvent> {

    public void previousSlide(final Object source) {
        final PreviousSlideEvent event = new PreviousSlideEvent(source);
        fireEvent(event);
    }

    @Override
    protected void fireEvent(final PreviousSlideEvent nextSlideEvent) {
        this.eventListeners.forEach(eventListener -> eventListener.previousSlide(nextSlideEvent));
    }
}
