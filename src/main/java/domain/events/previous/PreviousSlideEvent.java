package domain.events.previous;

import java.util.EventObject;

public class PreviousSlideEvent extends EventObject {
	private static final long serialVersionUID = -5655932714975316050L;

	/**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public PreviousSlideEvent(final Object source) {
        super(source);
    }
}
