package domain.events.previous;

import java.util.EventListener;

public interface PreviousSlideEventListener extends EventListener {
    void previousSlide(PreviousSlideEvent event);
}
