package domain.events;

import domain.events.update.SlideChangedEventDispatcher;

/**
 * SlideShowEventManager is a centralized controller which tells the view component to update itself whenever an event comes in.
 */
public class SlideShowEventManager extends EventManager {

    public final SlideChangedEventDispatcher slideChangedEventDispatcher;

    public SlideShowEventManager() {
        this.slideChangedEventDispatcher = new SlideChangedEventDispatcher();
    }
}
