package domain.events.next;

import java.util.EventObject;

public class NextSlideEvent extends EventObject {
	private static final long serialVersionUID = -1411351753977658481L;

	/**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public NextSlideEvent(final Object source) {
        super(source);
    }
}
