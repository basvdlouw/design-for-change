package domain.events.next;

import java.util.EventListener;

public interface NextSlideEventListener extends EventListener {
    void nextSlide(NextSlideEvent event);
}
