package domain.events.next;

import domain.events.EventDispatcher;

public class NextSlideEventDispatcher extends EventDispatcher<NextSlideEventListener, NextSlideEvent> {

    public void nextSlide(final Object source) {
        final NextSlideEvent event = new NextSlideEvent(source);
        this.fireEvent(event);
    }

    @Override
    protected void fireEvent(final NextSlideEvent nextSlideEvent) {
        this.eventListeners.forEach(eventListener -> eventListener.nextSlide(nextSlideEvent));
    }
}
