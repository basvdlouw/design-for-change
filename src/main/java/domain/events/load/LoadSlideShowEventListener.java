package domain.events.load;

import java.util.EventListener;

public interface LoadSlideShowEventListener extends EventListener {
    void loadSlideShow(LoadSlideShowEvent event);
}
