package domain.events.load;

import java.util.EventObject;

public class LoadSlideShowEvent extends EventObject {
	private static final long serialVersionUID = -8110530939858044507L;
	private final String filename;

    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public LoadSlideShowEvent(final Object source, final String filename) {
        super(source);
        this.filename = filename;
    }

    public String getFilename() {
        return this.filename;
    }
}
