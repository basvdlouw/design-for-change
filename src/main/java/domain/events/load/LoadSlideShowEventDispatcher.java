package domain.events.load;

import domain.events.EventDispatcher;

public class LoadSlideShowEventDispatcher extends EventDispatcher<LoadSlideShowEventListener, LoadSlideShowEvent> {

    public void loadSlideShow(final Object source, final String filename) {
        final LoadSlideShowEvent event = new LoadSlideShowEvent(source, filename);
        this.fireEvent(event);
    }

    @Override
    protected void fireEvent(final LoadSlideShowEvent loadSlideShowEvent) {
        this.eventListeners.forEach(eventListener -> eventListener.loadSlideShow(loadSlideShowEvent));
    }
}
