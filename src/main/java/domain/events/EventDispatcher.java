package domain.events;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.EventObject;

/**
 * EventDispatcher dispatches domain.events of type E to it's eventlisteners of type T
 * @param <T>
 * @param <E>
 */
public abstract class EventDispatcher<T extends EventListener, E extends EventObject> {

    protected final Collection<T> eventListeners;

    protected EventDispatcher() {
        this.eventListeners = new ArrayList<>();
    }

    public void addListener(final T eventListener) {
        this.eventListeners.add(eventListener);
    }

    public void addListeners(final Collection<T> eventListeners) {
        this.eventListeners.addAll(eventListeners);
    }

    public void removeListener(final T eventListener) {
        this.eventListeners.remove(eventListener);
    }

    protected abstract void fireEvent(E event);
}
