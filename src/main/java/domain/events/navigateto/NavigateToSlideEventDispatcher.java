package domain.events.navigateto;

import domain.events.EventDispatcher;

public class NavigateToSlideEventDispatcher extends EventDispatcher<NavigateToSlideEventListener, NavigateToSlideEvent> {

    public void goToSlide(final Object source, final int position) {
        final NavigateToSlideEvent event = new NavigateToSlideEvent(source, position);
        this.fireEvent(event);
    }

    @Override
    protected void fireEvent(final NavigateToSlideEvent navigateToEvent) {
        this.eventListeners.forEach(eventListener -> eventListener.toSlide(navigateToEvent));
    }
}
