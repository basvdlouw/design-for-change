package domain.events.navigateto;

import java.util.EventObject;

public class NavigateToSlideEvent extends EventObject {
	private static final long serialVersionUID = 3785940387035621731L;
	private final int position;

    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public NavigateToSlideEvent(final Object source, final int position) {
        super(source);
        this.position = position;
    }

    public int getPosition() {
        return this.position;
    }
}
