package domain.events.navigateto;

import java.util.EventListener;

public interface NavigateToSlideEventListener extends EventListener {
    void toSlide(NavigateToSlideEvent event);
}
