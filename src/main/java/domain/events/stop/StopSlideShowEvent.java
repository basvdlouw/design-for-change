package domain.events.stop;

import java.util.EventObject;

public class StopSlideShowEvent extends EventObject {
	private static final long serialVersionUID = 4954784514178461288L;

	/**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public StopSlideShowEvent(final Object source) {
        super(source);
    }
}
