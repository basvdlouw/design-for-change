package domain.events.stop;

import java.util.EventListener;

public interface StopSlideShowEventListener extends EventListener {
    void slideShowStopped(StopSlideShowEvent event);
}
