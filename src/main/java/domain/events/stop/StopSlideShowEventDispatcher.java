package domain.events.stop;

import domain.events.EventDispatcher;

public class StopSlideShowEventDispatcher extends EventDispatcher<StopSlideShowEventListener, StopSlideShowEvent> {

    public void slideShowStopped(final Object source) {
        final StopSlideShowEvent event = new StopSlideShowEvent(source);
        this.fireEvent(event);
    }

    @Override
    protected void fireEvent(final StopSlideShowEvent stopSlideShowEvent) {
        this.eventListeners.forEach(eventListener -> eventListener.slideShowStopped(stopSlideShowEvent));
    }
}
