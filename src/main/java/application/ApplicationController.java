package application;

import domain.builders.SimpleSlideShowBuilder;
import domain.builders.SimpleXMLBuilder;
import domain.builders.directors.XMLSlideShowConstructor;
import domain.events.SlideShowEventManager;
import domain.presentation.Presentation;
import domain.presentation.SlideShowPresentation;

/**
 * Facade pattern, creating all the required objects
 */
public class ApplicationController {

    public static final String TITLE = "Jabberpoint 1.6 - OU";

    private final SlideShowEventManager slideShowEventManager;
    private final Presentation presentation;

    public ApplicationController() {
        this.slideShowEventManager = new SlideShowEventManager();
        this.presentation = new SlideShowPresentation(
                new XMLSlideShowConstructor(
                        new SimpleSlideShowBuilder()
                ),
                new SimpleXMLBuilder(),
                this.slideShowEventManager
        );
    }

    public SlideShowEventManager getEventManager() {
        return slideShowEventManager;
    }

    public void start(final String fileName) {
        this.load(fileName);
        this.presentation.startPresentation();
    }

    public void stop() {
        this.presentation.stopPresentation();
    }

    public void load(final String fileName) {
        this.presentation.loadPresentation(fileName);
    }

    public void save(final String fileName) {
        this.presentation.savePresentation(fileName);
    }

    public void next() {
        this.presentation.nextSlide();
    }

    public void prev() {
        this.presentation.previousSlide();
    }

    public void goTo(final int index) {
        this.presentation.goToSlide(index);
    }
}
